package se.bbs.magic.poly.util

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentActivity

private const val RECORD_REQUEST_CODE = 101
private const val EXTERNAL_STORAGE_PERMISSIONS = Manifest.permission.WRITE_EXTERNAL_STORAGE

fun requestPermission(activity: FragmentActivity?, permission: String, requestCode: Int = RECORD_REQUEST_CODE) =
    ActivityCompat.requestPermissions(activity as Activity, arrayOf(permission), requestCode)

fun checkPermission(context: Context, activity: FragmentActivity?) {

    val storageRuntimePermission =
        ContextCompat.checkSelfPermission(context, EXTERNAL_STORAGE_PERMISSIONS)

    if (storageRuntimePermission == PackageManager.PERMISSION_DENIED) {
        println("Permission $EXTERNAL_STORAGE_PERMISSIONS was denied")

        requestPermission(activity, EXTERNAL_STORAGE_PERMISSIONS)
    }
}

fun onPermission(requestCode: Int, grantResults: IntArray, activity: FragmentActivity?) {
    when (requestCode) {
        RECORD_REQUEST_CODE -> {
            if (grantResults.isNotEmpty() && grantResults[0] ==
                PackageManager.PERMISSION_GRANTED
            ) {
                if ((ContextCompat.checkSelfPermission(
                        activity as Activity,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                    ) ==
                            PackageManager.PERMISSION_GRANTED)
                ) {
                    println("Permission ${Manifest.permission.WRITE_EXTERNAL_STORAGE} was granted")
                }
            }
        }
    }
}