package se.bbs.magic.poly.repository.uility

import se.bbs.magic.poly.model.Category
import se.bbs.magic.poly.model.db.entity.Format
import se.bbs.magic.poly.model.db.entity.Polygon
import se.bbs.magic.poly.model.db.entity.Resource
import se.bbs.magic.poly.model.network.Asset
import java.lang.IllegalArgumentException
import java.util.*
import java.util.stream.Collectors.toList
/**
 * This resource should not exists.
 * need to learn Android ROOM
 * */
private val assetToPolygon = { asset: Asset, category: Category ->
    Polygon(
        id = asset.name,
        authorName = asset.authorName,
        displayName = asset.displayName,
        description = asset.description,
        license = asset.license,
        createTime = asset.createTime,
        updateTime = asset.updateTime,
        type = category,
        thumbnail = asset.thumbnail
    )
}

private val toFormat = {polygonId: String, formats: List<se.bbs.magic.poly.model.network.Format> ->
    formats.stream()
        .map {
            Pair(
                Format(

                    id = UUID.randomUUID().toString(),
                    polygonId = polygonId,
                    formatType = it.formatType,
                    path = null,
                    root = it.root
                ),
                it.resources
            )
        }
        .collect(toList())
}

private val toResource = { formatId: String, resources: List<se.bbs.magic.poly.model.network.Resource>? ->
    resources
        ?.map {
            Resource(
                id = null,
                formatId = formatId,
                contentType = it.contentType,
                relativePath = it.relativePath,
                url = it.url,
                path = null
            )
        }
}


private fun transformResponseParts(asset: Asset, category: Category): Triple<Polygon, List<Format>, List<Resource>> {
    val polygon = assetToPolygon(asset, category)
    val flattenFormatAndResource: MutableList<Pair<Format, List<se.bbs.magic.poly.model.network.Resource>?>>? =  toFormat(polygon.id, asset.formats)
    val format = flattenFormatAndResource?.map { it.first }!!
    val resource = flattenFormatAndResource.map { toResource(it.first.id, it.second) }
        .flatMap { it ?: emptyList() }

    return Triple(polygon, format, resource)
}

fun transformToEntity(assets: Optional<List<Asset>>, category: Category): MutableList<Triple<Polygon, List<Format>, List<Resource>>>? {
    return assets.orElseThrow { IllegalArgumentException() }
        .stream()
        .map { transformResponseParts(it, category) }
        .collect(toList())
}