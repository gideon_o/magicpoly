package se.bbs.magic.poly.configuration

import android.content.Context
import retrofit2.Retrofit
import se.bbs.magic.poly.BuildConfig
import se.bbs.magic.poly.configuration.network.FileResolver
import se.bbs.magic.poly.configuration.network.PolygonHttpClient
import se.bbs.magic.poly.configuration.network.RetrofitClient
import se.bbs.magic.poly.repository.PolygonRepository
import java.net.URL

fun provideRetrofitClient(): Retrofit
        = RetrofitClient.getInstance(URL(BuildConfig.BASE_URL))

fun providePolygonHttpClient(): PolygonHttpClient
        = provideRetrofitClient().create(PolygonHttpClient::class.java)

fun provideDataSource(context: Context): DataSource
        = DataSource.getInstance(context)

fun providePolygonRepository(context: Context): PolygonRepository
        = PolygonRepository(provideDataSource(context).providePolygonApiDao(), providePolygonHttpClient())

fun provideArCorHandler(context: Context): ARCoreAssetHandler
        = ARCoreAssetHandler(context)

fun provideFileResolver(context: Context): FileResolver
        = FileResolver(context)