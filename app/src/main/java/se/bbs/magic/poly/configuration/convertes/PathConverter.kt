package se.bbs.magic.poly.configuration.convertes

import androidx.room.TypeConverter
import com.google.gson.*
import java.lang.reflect.Type
import java.nio.file.Path
import java.nio.file.Paths

class PathConverter: JsonSerializer<Path>, JsonDeserializer<Path> {

    @TypeConverter
    fun toPath(uri: String?): Path? {
        return if (uri == null) null else Paths.get(uri)
    }

    @TypeConverter
    fun fromUrl(path: Path?): String {
        return path?.toString() ?: ""
    }

    override fun serialize(src: Path?, typeOfSrc: Type?, context: JsonSerializationContext?): JsonElement {
        return JsonPrimitive(src.toString())
    }

    override fun deserialize(json: JsonElement?, typeOfT: Type?, context: JsonDeserializationContext?): Path? {
        return toPath(json!!.asString)
    }
}