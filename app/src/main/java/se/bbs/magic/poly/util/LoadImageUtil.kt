package se.bbs.magic.poly.util

import android.content.Context
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.BitmapImageViewTarget


fun loadImage(context: Context, source: String, imageView: ImageView) {
    Glide.with(context)
        .load(source)
        .into(imageView)
}

fun loadImageOnDispatchThread(context: Context, source: String, imageView: ImageView) {
    Glide.with(context)
        .asBitmap()
        .load(source)
        .into(BitmapImageViewTarget(imageView))
}