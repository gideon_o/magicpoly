package se.bbs.magic.poly.configuration

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import se.bbs.magic.poly.BuildConfig
import se.bbs.magic.poly.configuration.convertes.CategoryConverter
import se.bbs.magic.poly.configuration.convertes.InstantConverter
import se.bbs.magic.poly.configuration.convertes.PathConverter
import se.bbs.magic.poly.configuration.pattern.Singleton
import se.bbs.magic.poly.dao.PolygonDao
import se.bbs.magic.poly.model.db.entity.Format
import se.bbs.magic.poly.model.db.entity.Page
import se.bbs.magic.poly.model.db.entity.Polygon
import se.bbs.magic.poly.model.db.entity.Resource
import se.bbs.magic.poly.model.db.ui.PolygonListEntry

@Database(
    entities = [
        Polygon::class,
        Format::class,
        Resource::class,
        Page::class,
        PolygonListEntry::class
    ],
    version = 1,
    exportSchema = false
)
@TypeConverters(CategoryConverter::class, InstantConverter::class, PathConverter::class)
abstract class DataSource: RoomDatabase() {

    abstract fun providePolygonApiDao(): PolygonDao

    companion object: Singleton<Context, DataSource>({
        Room.databaseBuilder(it.applicationContext, DataSource::class.java, BuildConfig.DATABASE_NAME)
            .build()
    })
}
