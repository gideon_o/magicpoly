package se.bbs.magic.poly.model.db.entity

import androidx.room.ColumnInfo
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import se.bbs.magic.poly.model.Category
import se.bbs.magic.poly.model.network.Thumbnail
import java.time.Instant

@Entity(tableName = "polygon")
data class Polygon(@PrimaryKey                                  val id           : String,
                   @ColumnInfo(name         = "author"        ) val authorName   : String,
                   @ColumnInfo(name         = "name"          ) val displayName  : String,
                   @ColumnInfo(name         = "description"   ) val description  : String?,
                   @ColumnInfo(name         = "license"       ) val license      : String,
                   @ColumnInfo(name         = "created"       ) val createTime   : Instant,
                   @ColumnInfo(name         = "updated"       ) val updateTime   : Instant,
                   @ColumnInfo(name         = "type"          ) val type         : Category?,
                   @Embedded                                    val thumbnail    : Thumbnail?
)