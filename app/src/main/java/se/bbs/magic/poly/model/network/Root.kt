package se.bbs.magic.poly.model.network


import com.google.gson.annotations.SerializedName

data class Root(
    val contentType: String,
    val relativePath: String,
    val url: String
)