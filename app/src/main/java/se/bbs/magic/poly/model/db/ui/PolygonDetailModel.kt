package se.bbs.magic.poly.model.db.ui

import se.bbs.magic.poly.model.db.entity.Format
import se.bbs.magic.poly.model.db.entity.Polygon

class PolygonDetailModel private constructor(
    val polygon: Polygon?,
    val formats: List<Format>?
   ) {

    data class Builder(
        var polygon: Polygon? = null,
        var formats: List<Format>? = null
    ) {

        fun polygon(polygon: Polygon) = apply { this.polygon = polygon }

        fun formats(formats: List<Format>) = apply { this.formats = formats }

        fun build() = PolygonDetailModel(polygon, formats)
    }
}