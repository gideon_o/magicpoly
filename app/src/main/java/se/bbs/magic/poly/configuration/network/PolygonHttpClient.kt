package se.bbs.magic.poly.configuration.network

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query
import se.bbs.magic.poly.BuildConfig
import se.bbs.magic.poly.model.Category
import se.bbs.magic.poly.model.network.PolygonApiResponse

interface PolygonHttpClient {
    @GET("/v1/assets")
    fun getCategories(
        @Query("category") category: Category,
        @Query("pageToken") token: String? = null,
        @Query("key") key: String = BuildConfig.KEY,
        @Query("pageSize") pageSize: Int = BuildConfig.PAGE_COUNT
    ): Call<PolygonApiResponse>
}