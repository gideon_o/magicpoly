package se.bbs.magic.poly.model.db.entity

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.Index
import androidx.room.PrimaryKey
import java.nio.file.Path

@Entity(tableName   = "resource",
        indices     = [ Index("formatId") ],
        foreignKeys = [ ForeignKey(entity        = Format::class,
                                   parentColumns = arrayOf("id"),
                                   childColumns  = arrayOf("formatId"),
                                   onDelete      = ForeignKey.CASCADE) ])
data class Resource(
    @PrimaryKey(autoGenerate = true) val id: Int?,
    val formatId: String,
    val contentType: String,
    val relativePath: String,
    val url: String,
    val path: Path?
)