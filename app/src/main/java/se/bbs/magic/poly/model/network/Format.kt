package se.bbs.magic.poly.model.network


import com.google.gson.annotations.SerializedName

data class Format(
    val formatComplexity: FormatComplexity,
    val formatType: String,
    val resources: List<Resource>?,
    val root: Root
)