package se.bbs.magic.poly.configuration.network

import android.content.Context
import android.os.Environment
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.widget.Toast
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import se.bbs.magic.poly.model.db.entity.Format
import se.bbs.magic.poly.model.db.entity.Polygon
import java.io.File
import java.io.FileOutputStream
import java.net.URL
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import java.util.*


class FileResolver(
    private val context: Context
) {
    private val tag = FileResolver::class.java.name
    private val baseStorageDir = context.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS)

    private fun getFile(format: Format, path: Path): Path
            = Paths.get(path.toString(), format.root.relativePath)

    private fun download(
        url: String,
        filePath: Path
    ): String {

        Log.i(tag, "attempting to download: $filePath")

        URL(url).openStream().use { input ->
            FileOutputStream(File(filePath.toString())).use { output ->
                input.copyTo(output)
                Handler(Looper.getMainLooper()).post(Runnable {
                    Toast.makeText(context, "Asset downloaded...", Toast.LENGTH_LONG).show()
                })
            }
        }

        return filePath.toString()
    }

    private fun getSubPath(polygonId: String, formatType: String): Path {
        val subPath = Paths.get(
            baseStorageDir?.absolutePath!!,
            polygonId,
            formatType
        ).toAbsolutePath()

        if (Files.notExists(subPath)) {
            Log.i(tag, "Creating sub path $subPath")
            Files.createDirectories(subPath)
        }

        return subPath
    }

    fun renderModel(
        polygon: Polygon,
        format: Format,
        onRender: (String) -> Unit,
        onSavePath: (String, String, String) -> String
    ) {
        GlobalScope.launch(Dispatchers.IO) {
            val filePath = getFile(format, getSubPath(polygon.id, format.formatType))

            val path = format.path ?: onSavePath(polygon.id, format.id, download(format.root.url, filePath))

            onRender(format.root.url)
        }
    }
}
