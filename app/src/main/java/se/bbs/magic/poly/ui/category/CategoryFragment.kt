package se.bbs.magic.poly.ui.category

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.Navigation
import se.bbs.magic.poly.R
import se.bbs.magic.poly.model.Category

class CategoryFragment : Fragment() {

    private lateinit var navController: NavController

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_category, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        navController = Navigation.findNavController(view)

        val animalBtn = view.findViewById<Button>(R.id.btn_category_animals)

        val bundle = bundleOf("category" to Category.ANIMALS.name)

        animalBtn.setOnClickListener { navController.navigate(R.id.action_categoryFragment_to_polygonFragment, bundle) }
    }
}