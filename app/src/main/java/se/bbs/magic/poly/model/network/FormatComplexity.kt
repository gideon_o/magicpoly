package se.bbs.magic.poly.model.network


import com.google.gson.annotations.SerializedName

data class FormatComplexity(
    val triangleCount: String
)