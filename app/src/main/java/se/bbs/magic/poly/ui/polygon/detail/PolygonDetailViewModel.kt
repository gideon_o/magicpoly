package se.bbs.magic.poly.ui.polygon.detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import se.bbs.magic.poly.model.db.ui.PolygonDetailModel
import se.bbs.magic.poly.repository.PolygonRepository

class PolygonDetailViewModel(
    private val polygonRepository: PolygonRepository
) : ViewModel() {

    lateinit var polygonId: String

    private var polygonDetailViewModel: MutableLiveData<PolygonDetailModel> = MutableLiveData()

    private lateinit var polygonDetailModel: PolygonDetailModel

    fun init(id: String) = GlobalScope.launch(Dispatchers.IO) {
        polygonId = id

        //need to rethink underlying models, to avoid double call to db
        polygonDetailModel = PolygonDetailModel.Builder()
            .polygon(polygonRepository.getPolygon(id)!!)
            .formats(polygonRepository.getFormat(id)!!)
            .build()

        polygonDetailViewModel.postValue(polygonDetailModel)
    }

    fun getViewModel(): LiveData<PolygonDetailModel> =
        polygonDetailViewModel

    fun saveFormatPath(polygonId: String, formatId: String, path: String) {
        polygonRepository.updateFormatPath(formatId, path)

        polygonDetailModel = PolygonDetailModel.Builder()
            .polygon(polygonDetailModel.polygon!!)
            .formats(polygonRepository.getFormat(polygonId)!!)
            .build()

        polygonDetailViewModel.postValue(polygonDetailModel)
    }
}
