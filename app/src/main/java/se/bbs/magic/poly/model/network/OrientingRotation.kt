package se.bbs.magic.poly.model.network


import com.google.gson.annotations.SerializedName

data class OrientingRotation(
    val w: Double
)