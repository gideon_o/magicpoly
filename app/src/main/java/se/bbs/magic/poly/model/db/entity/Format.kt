package se.bbs.magic.poly.model.db.entity


import androidx.room.*
import se.bbs.magic.poly.model.network.Root
import java.nio.file.Path

@Entity(tableName   = "format",
        indices     = [ Index("polygonId") ],
        foreignKeys = [ ForeignKey(entity        = Polygon::class,
                                   parentColumns = arrayOf("id"),
                                   childColumns  = arrayOf("polygonId"),
                                   onDelete      = ForeignKey.CASCADE) ])
data class Format(
    @PrimaryKey val id: String,
    val polygonId: String,
    val formatType: String,
    val path: String?,
    @Embedded
    val root: Root
)