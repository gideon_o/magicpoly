package se.bbs.magic.poly.model.db.ui

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class PolygonListEntry(
    @PrimaryKey val id: Int?,
    val assetName: String,
    val name: String,
    val author: String,
    val url: String
)