package se.bbs.magic.poly.configuration.pattern

import java.util.*

abstract class Singleton<in T: Any, out R>(creator: (T) -> R ) {

    @Volatile
    private var instance: R? = null

    val construct = { args: T -> synchronized(this) {
        Optional.ofNullable(instance)
            .orElseGet { instance = creator(args); instance }
    }
    }

    fun getInstance(args: T): R {
        return Optional.ofNullable(instance)
            .orElseGet { construct(args) }
    }
}