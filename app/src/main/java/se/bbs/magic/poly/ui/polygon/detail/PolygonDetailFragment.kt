package se.bbs.magic.poly.ui.polygon.detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import se.bbs.magic.poly.R
import se.bbs.magic.poly.configuration.ARCoreAssetHandler
import se.bbs.magic.poly.configuration.network.FileResolver
import se.bbs.magic.poly.configuration.provideArCorHandler
import se.bbs.magic.poly.configuration.provideFileResolver
import se.bbs.magic.poly.configuration.providePolygonRepository
import se.bbs.magic.poly.model.db.entity.Format
import se.bbs.magic.poly.model.db.entity.Polygon
import se.bbs.magic.poly.util.checkPermission
import se.bbs.magic.poly.util.loadImageOnDispatchThread
import se.bbs.magic.poly.util.onPermission
import java.util.*

class PolygonDetailFragment : Fragment() {
    private val polygonDetailViewModel: PolygonDetailViewModel by viewModels {
        PolygonDetailViewModelFactory(providePolygonRepository(requireContext()))
    }

    private lateinit var fileResolver: FileResolver

    private lateinit var arCoreAssetHandler: ARCoreAssetHandler

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        fileResolver = provideFileResolver(requireContext())

        arCoreAssetHandler = provideArCorHandler(requireContext())

        polygonDetailViewModel.init(
            arguments?.getString("id")!!
        )

        checkPermission(requireContext(), activity)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        onPermission(requestCode, grantResults, activity)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val polygonDetailFragment =
            inflater.inflate(R.layout.fragment_polygon_detail, container, false)

        val btnLayout =
            polygonDetailFragment.findViewById<LinearLayout>(R.id.render_btn_container)

        polygonDetailViewModel.getViewModel().observe(viewLifecycleOwner, Observer {
            val polygon = it.polygon
            val formats = it.formats

            if (polygon != null && formats != null) {
                removeRenderBtn(formats, btnLayout)

                initViewComponents(polygon, polygonDetailFragment)

                initRenderButtons(
                    polygon,
                    formats,
                    btnLayout
                )
            }
        })

        return polygonDetailFragment
    }

    private fun removeRenderBtn(formats: List<Format>, btnLayout: LinearLayout) =
        formats.map {
            Optional.ofNullable(btnLayout.findViewWithTag<Button>(it.id))
                .ifPresent { btn -> btnLayout.removeView(btn) }
        }

    private fun initViewComponents(polygon: Polygon, polygonDetailFragment: View) {

        val imageView = polygonDetailFragment.findViewById<ImageView>(R.id.polygon_detail_image)

        loadImageOnDispatchThread(
            requireContext(),
            polygon.thumbnail!!.url,
            imageView
        )

        setTextValue(polygon, polygonDetailFragment)
    }

    private fun setTextValue(polygon: Polygon, polygonDetailFragment: View) {
        hashMapOf(
            R.id.polygon_detail_name to polygon.displayName,
            R.id.polygon_detail_author to polygon.authorName,
            R.id.polygon_detail_text to polygon.description
        ).entries.forEach {
            polygonDetailFragment.findViewById<TextView>(it.key).apply {
                text = it.value?: "N/A"
            }
        }
    }

    private val onRender = { path: String -> arCoreAssetHandler.renderModel(path) }

    private val onSavePath = {
            polygonId: String, formatId: String, path: String ->
        polygonDetailViewModel.saveFormatPath(polygonId, formatId, path); path
    }

    private fun initRenderButtons(polygon: Polygon, formats: List<Format>, layout: LinearLayout) {

        formats.map { format ->
            val btn = Button(requireContext()).apply {
                this.text = format.formatType
                this.tag = format.id
                this.setOnClickListener { fileResolver.renderModel(polygon, format, onRender, onSavePath) }
                this.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_visibility_24px, 0)
            }

            layout.addView(btn)
        }
    }
}