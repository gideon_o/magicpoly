package se.bbs.magic.poly.ui.polygon.detail

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import se.bbs.magic.poly.repository.PolygonRepository
import java.lang.IllegalArgumentException

@Suppress("UNCHECKED_CAST")
class PolygonDetailViewModelFactory(
    private val polygonRepository: PolygonRepository
): ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(PolygonDetailViewModel::class.java)) {
            return PolygonDetailViewModel(polygonRepository) as T
        }

        throw IllegalArgumentException("Unknown view model was provided ")
    }
}