package se.bbs.magic.poly

import android.os.Bundle
import android.os.Handler
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.ar.core.ArCoreApk
import com.google.ar.core.Session
import com.google.ar.core.exceptions.UnavailableUserDeclinedInstallationException
import se.bbs.magic.poly.util.CameraPermissionHelper
import se.bbs.magic.poly.util.CameraPermissionHelper.hasCameraPermission
import se.bbs.magic.poly.util.CameraPermissionHelper.launchPermissionSettings
import se.bbs.magic.poly.util.CameraPermissionHelper.shouldShowRequestPermissionRationale


class MainActivity : AppCompatActivity() {
    // Set to true ensures requestInstall() triggers installation if necessary.
    private var mUserRequestedInstall = true

    private var mSession: Session? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        arSupportCheck()
    }

    private fun arSupportCheck() {
        val availability = ArCoreApk.getInstance().checkAvailability(this)
        if (availability.isTransient) {
            // Re-query at 5Hz while compatibility is checked in the background.
            Handler().postDelayed(Runnable { arSupportCheck() }, 200)
        }
        if (availability.isSupported) {
            Toast.makeText(this, "ARCore is supported on your device", Toast.LENGTH_LONG).show()
        } else {
            Toast.makeText(this, "ARCore is not supported on your device", Toast.LENGTH_LONG).show()
        }
    }

    override fun onResume() {
        super.onResume()

        // ARCore requires camera permission to operate.
        if (!hasCameraPermission(this)) {
            CameraPermissionHelper.requestCameraPermission(this)
            return
        }

        try {
            if (mSession == null) {
                when (ArCoreApk.getInstance().requestInstall(this, mUserRequestedInstall)) {
                    ArCoreApk.InstallStatus.INSTALLED ->           // Success, create the AR session.
                        mSession = Session(this)
                    ArCoreApk.InstallStatus.INSTALL_REQUESTED -> {
                        // Ensures next invocation of requestInstall() will either return
                        // INSTALLED or throw an exception.
                        mUserRequestedInstall = false
                        return
                    }
                }
            }
        } catch (e: UnavailableUserDeclinedInstallationException) {
            Toast.makeText(this, "TODO: handle exception $e", Toast.LENGTH_LONG)
                .show()
            return
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if (!hasCameraPermission(this)) {
            Toast.makeText(
                this,
                "Camera permission is needed to run this application",
                Toast.LENGTH_LONG
            )
                .show()
            if (!shouldShowRequestPermissionRationale(this)) {
                // Permission denied with checking "Do not ask again".
                launchPermissionSettings(this)
            }
            finish()
        }
    }
}