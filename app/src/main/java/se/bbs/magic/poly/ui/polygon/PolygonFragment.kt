package se.bbs.magic.poly.ui.polygon

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import se.bbs.magic.poly.R
import se.bbs.magic.poly.configuration.providePolygonRepository
import se.bbs.magic.poly.model.Category

class PolygonFragment : Fragment() {

    private lateinit var category: Category
    private lateinit var polygonEntryAdapter: PolygonEntryRecyclerAdapter

    private val polygonViewModel : PolygonViewModel by viewModels {
        PolygonViewModelFactory(providePolygonRepository(requireContext()))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        category = Category.valueOf(arguments?.getString("category")!!)

        polygonEntryAdapter = PolygonEntryRecyclerAdapter()

        polygonViewModel.init(category)

        polygonViewModel.getViewModel().observe(this, Observer {
            println(it?.size)
            if (it.isNotEmpty()) {
                polygonEntryAdapter.submitList(it)
                polygonEntryAdapter.notifyDataSetChanged()
            }
        })
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val polygonFragment =  inflater.inflate(R.layout.fragment_polygon, container, false)

        val polygonEntryRecyclerView = polygonFragment.findViewById<RecyclerView>(R.id.polygon_list_recycler_view)

        polygonViewModel.getLoadingViewModel().observe(viewLifecycleOwner, Observer {
            polygonFragment.findViewById<ProgressBar>(R.id.polygon_progress_bar).apply {
                visibility = if (it) View.VISIBLE else View.GONE
            }
        })

        initRecyclerView(polygonEntryRecyclerView)

        return polygonFragment
    }

    private fun initRecyclerView(recyclerView: RecyclerView) {
        recyclerView.apply {
            layoutManager = LinearLayoutManager(this@PolygonFragment.requireContext())
            adapter = polygonEntryAdapter
        }
    }
}