package se.bbs.magic.poly.configuration.network

import com.google.gson.GsonBuilder
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import se.bbs.magic.poly.configuration.convertes.InstantConverter
import se.bbs.magic.poly.configuration.pattern.Singleton
import java.lang.reflect.Type
import java.net.URL
import java.nio.file.Path
import java.time.Instant

abstract class RetrofitClient {

    companion object: Singleton<URL, Retrofit>({

        val typeConverters = GsonBuilder()
            .registerTypeAdapter(Instant::class.java, InstantConverter())
//            .registerTypeAdapter(Path::class.java, PathConverter())
            .create()

        retrofit2.Retrofit.Builder()
            .baseUrl(it) // should be provided when creating client
            .addConverterFactory(GsonConverterFactory.create(typeConverters))
            .build()
    })
}