package se.bbs.magic.poly.configuration

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.util.Log
import androidx.core.content.ContextCompat.startActivity
import java.util.*


class ARCoreAssetHandler(
    private val context: Context
) {

    companion object {
        val SUPPORT_FORMATS =
            listOf("glTF2").map { it.toUpperCase(Locale.ROOT) }

        val AR_CORE_HANDLER_TAG = ARCoreAssetHandler::class.java.name
    }

    fun renderModel(path: String) {
        Log.i(AR_CORE_HANDLER_TAG, "rendering model: $path")
        val sceneViewerIntent = Intent(Intent.ACTION_VIEW)
        val intentUri =
            Uri.parse("https://arvr.google.com/scene-viewer/1.0").buildUpon()
                .appendQueryParameter("file", path)
                .appendQueryParameter("mode", "ar_only")
                .build()
        sceneViewerIntent.data = intentUri
        sceneViewerIntent.setPackage("com.google.ar.core")
        startActivity(context, sceneViewerIntent, null)
    }
}