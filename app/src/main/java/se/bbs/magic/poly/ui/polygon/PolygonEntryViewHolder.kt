package se.bbs.magic.poly.ui.polygon

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.polygon_list_item.view.*
import se.bbs.magic.poly.model.db.ui.PolygonListEntry
import se.bbs.magic.poly.util.loadImage

class PolygonEntryViewHolder constructor(
    itemView: View
) : RecyclerView.ViewHolder(itemView) {
    private val thumbnailImage = itemView.polygon_entry_thumbnail
    private val name = itemView.polygon_entry_name
    private val author = itemView.polygon_entry_author

    fun bind(entry: PolygonListEntry) {
        name.text = entry.name
        author.text = entry.author

        loadImage(
            itemView.context,
            entry.url,
            thumbnailImage
        )
    }
}