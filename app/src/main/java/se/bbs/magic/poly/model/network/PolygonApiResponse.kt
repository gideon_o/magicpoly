package se.bbs.magic.poly.model.network


import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "current_page")
data class PolygonApiResponse(
    @Ignore val assets: List<Asset>,
    @PrimaryKey val nextPageToken: String,
    val totalSize: Int
)