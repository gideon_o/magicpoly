package se.bbs.magic.poly.repository

import retrofit2.awaitResponse
import se.bbs.magic.poly.BuildConfig
import se.bbs.magic.poly.configuration.network.PolygonHttpClient
import se.bbs.magic.poly.dao.PolygonDao
import se.bbs.magic.poly.model.Category
import se.bbs.magic.poly.model.db.entity.Format
import se.bbs.magic.poly.model.db.entity.Page
import se.bbs.magic.poly.model.db.entity.Polygon
import se.bbs.magic.poly.model.db.entity.Resource
import se.bbs.magic.poly.model.db.ui.PolygonListEntry
import se.bbs.magic.poly.model.network.PolygonApiResponse
import se.bbs.magic.poly.repository.uility.transformToEntity
import java.util.*

/**
 * TODO handle logic for persisting data,
 * PolygonHttpClient,
 * PolygonDao <- not implemented yet
 *
 * PolygonHttpClient --- \
 *                          PolygonRepository -> PolygonViewModel -> PolygonFragment
 * PolygonDao        --- /
 * */

class PolygonRepository(
    private val polygonDao: PolygonDao,
    private val polygonHttpClient: PolygonHttpClient
) {

    private val pageSize = BuildConfig.PAGE_COUNT
    private var nextPageToken: String? = null

    suspend fun init(category: Category): List<PolygonListEntry> {
        val currentPage = polygonDao.getCurrentPage(category)
        if (currentPage != null) {
            nextPageToken = currentPage.token
            return polygonDao.getPolygonListEntry()
        }
        return load(category)
    }

    suspend fun load(category: Category): List<PolygonListEntry> {
        return savePolygon(polygonHttpClient
            .getCategories(category, token = nextPageToken)
            .awaitResponse()
            .body(), category)
    }

    private fun savePolygon(response: PolygonApiResponse?, category: Category): List<PolygonListEntry> {
        nextPageToken = response?.nextPageToken

        val asset = Optional.ofNullable(response?.assets)

        val entities = Optional.ofNullable(transformToEntity(asset, category))

        if (nextPageToken != null) {
            polygonDao.insertPage(
                Page(
                    nextPageToken!!,
                    response?.totalSize,
                    category
                )
            )
        }

        entities.map {
            it.forEach { triple ->
                polygonDao.insertPolygon(triple.first)
                polygonDao.insertFormat(triple.second)
                polygonDao.insertResource(triple.third)
            }
        }

        return polygonDao.getPolygonListEntry()
    }

    fun getPolygon(id: String): Polygon? = polygonDao.getPolygonById(id)

    fun getFormat(polygonId: String): List<Format>? = polygonDao.getFormatByPolygonId(polygonId)

    fun getResource(formatId: String): List<Resource>? = polygonDao.getResourceByFormatId(formatId)

    fun updateFormatPath(formatId: String, path: String) = polygonDao.updateFormPath(formatId, path)
}