package se.bbs.magic.poly.model.network


import com.google.gson.annotations.SerializedName

data class Resource(
    val formatId: Int,
    val contentType: String,
    val relativePath: String,
    val url: String
)