package se.bbs.magic.poly.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import se.bbs.magic.poly.BuildConfig
import se.bbs.magic.poly.configuration.ARCoreAssetHandler.Companion.SUPPORT_FORMATS
import se.bbs.magic.poly.model.Category
import se.bbs.magic.poly.model.db.entity.Format
import se.bbs.magic.poly.model.db.entity.Page
import se.bbs.magic.poly.model.db.entity.Polygon
import se.bbs.magic.poly.model.db.entity.Resource
import se.bbs.magic.poly.model.db.ui.PolygonListEntry
import java.nio.file.Path

@Dao
interface PolygonDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertPolygon(polygon: Polygon)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertFormat(format: List<Format>)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertResource(resource: List<Resource>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertPage(page: Page)

    @Query("SELECT * FROM page WHERE category = :category ORDER BY token DESC LIMIT 1")
    fun getCurrentPage(category: Category): Page?

    @Query("SELECT id as assetName, name, author, url FROM polygon LIMIT :limit OFFSET :offset")
    fun getPolygonListEntry(limit: Int = BuildConfig.PAGE_COUNT, offset: Int = 0): List<PolygonListEntry>

    @Query("SELECT * FROM polygon WHERE id = :id")
    fun getPolygonById(id: String): Polygon?

    @Query("SELECT * FROM format WHERE polygonId = :polygonId AND formatType IN (:supportedTypes)")
    fun getFormatByPolygonId(polygonId: String, supportedTypes: List<String> = SUPPORT_FORMATS): List<Format>?

    @Query("SELECT * FROM resource WHERE formatId = :formatId")
    fun getResourceByFormatId(formatId: String): List<Resource>?

    @Query("UPDATE format SET path=:path WHERE id = :formatId")
    fun updateFormPath(formatId: String, path: String): Int
}