package se.bbs.magic.poly.model.network


import com.google.gson.annotations.SerializedName

data class PresentationParams(
    val backgroundColor: String,
    val colorSpace: String,
    val orientingRotation: OrientingRotation
)