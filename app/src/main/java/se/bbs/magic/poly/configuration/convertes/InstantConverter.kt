package se.bbs.magic.poly.configuration.convertes

import androidx.room.TypeConverter
import com.google.gson.*
import java.lang.reflect.Type
import java.time.Instant
import java.time.format.DateTimeFormatter

class InstantConverter: JsonSerializer<Instant>, JsonDeserializer<Instant> {

    private val formatter = DateTimeFormatter.ISO_INSTANT

    @TypeConverter
    fun toInstant(timestamp: Long?): Instant? {
        return if (timestamp == null) null else Instant.ofEpochSecond(timestamp)
    }

    @TypeConverter
    fun toTimestamp(instant: Instant?): Long? {
        return instant?.toEpochMilli()
    }

    override fun serialize(src: Instant?, typeOfSrc: Type?, context: JsonSerializationContext?): JsonElement {
        return JsonPrimitive(formatter.format(src))
    }

    override fun deserialize(json: JsonElement?, typeOfT: Type?, context: JsonDeserializationContext?): Instant {
        if (json == null) {
            return Instant.EPOCH
        }
        return formatter.parse(json.asString, Instant::from)
    }
}