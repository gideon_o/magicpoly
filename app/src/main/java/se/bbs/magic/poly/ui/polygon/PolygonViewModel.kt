package se.bbs.magic.poly.ui.polygon

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import se.bbs.magic.poly.model.Category
import se.bbs.magic.poly.model.db.ui.PolygonListEntry
import se.bbs.magic.poly.repository.PolygonRepository

/**
 * TODO
 * Should Mutable data structures i.e MutableLiveData<PolygonListEntry>
 * viewModel exposes method to mutate the state. But the data is only
 * exposed as LiveData<PolygonListEntry> as LiveDate is immutable.
 * */

class PolygonViewModel(
    private val polygonRepository: PolygonRepository
): ViewModel() {
    private var polygonEntries: MutableLiveData<List<PolygonListEntry>> = MutableLiveData()

    private var isLoading: MutableLiveData<Boolean> = MutableLiveData(true)

    fun getViewModel(): LiveData<List<PolygonListEntry>>
            = polygonEntries

    fun getLoadingViewModel(): MutableLiveData<Boolean>
            = isLoading

    fun init(category: Category) = GlobalScope.launch(Dispatchers.IO) {
        polygonEntries.postValue(polygonRepository.init(category))
        isLoading.postValue(false)
    }

    fun loadNextPage(category: Category) = GlobalScope.launch(Dispatchers.IO) {
        isLoading.postValue(true)
        polygonEntries.postValue(polygonRepository.load(category))
        isLoading.postValue(false)
    }
}