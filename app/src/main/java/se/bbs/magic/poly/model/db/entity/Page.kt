package se.bbs.magic.poly.model.db.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import se.bbs.magic.poly.model.Category

@Entity(tableName = "page")
data class Page(
    @PrimaryKey val token: String,
    val totalSize: Int?,
    val category: Category
)