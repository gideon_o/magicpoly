package se.bbs.magic.poly.model.network


import com.google.gson.annotations.SerializedName
import java.time.Instant

data class Asset(
    val authorName: String,
    val createTime: Instant,
    val description: String?,
    val displayName: String,
    val formats: List<Format>,
    val isCurated: Boolean,
    val license: String,
    val name: String,
    val presentationParams: PresentationParams,
    val thumbnail: Thumbnail,
    val updateTime: Instant,
    val visibility: String
)