package se.bbs.magic.poly.ui.polygon

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.navigation.findNavController
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import se.bbs.magic.poly.R
import se.bbs.magic.poly.model.db.ui.PolygonListEntry

class PolygonEntryRecyclerAdapter : ListAdapter<PolygonListEntry, PolygonEntryViewHolder>(object :
    DiffUtil.ItemCallback<PolygonListEntry>() {
    override fun areItemsTheSame(
        oldItem: PolygonListEntry,
        newItem: PolygonListEntry
    ): Boolean {
        return oldItem.assetName == newItem.assetName
    }

    override fun areContentsTheSame(
        oldItem: PolygonListEntry,
        newItem: PolygonListEntry
    ): Boolean {
        return areContentsTheSame(oldItem, newItem)
    }
}) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PolygonEntryViewHolder {
        return PolygonEntryViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.polygon_list_item, parent, false)
        )
    }

    override fun onBindViewHolder(holder: PolygonEntryViewHolder, position: Int) {
        val entry = getItem(position)

        val bundle = bundleOf("id" to entry.assetName)

        holder.itemView.setOnClickListener { view ->
            view.findNavController()
                .navigate(R.id.action_polygonFragment_to_polygonDetailFragment, bundle) }

        holder.bind(entry)
    }
}
