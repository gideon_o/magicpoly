package se.bbs.magic.poly.configuration.convertes

import androidx.room.TypeConverter
import se.bbs.magic.poly.model.Category

class CategoryConverter {

    @TypeConverter
    fun toPolyCategory(type: String?): Category? {
        return type?.run { Category.valueOf(this) }
    }

    @TypeConverter
    fun toString(type: Category?): String? {
        return type?.toString()
    }
}