package se.bbs.magic.poly.model

enum class Category constructor(var category: String) {
    ANIMALS("animals"),
    ARCHITECTURE("architecture"),
    ART("art"),
    FOOD("food"),
    NATURE("nature"),
    OBJECTS("objects"),
    PEOPLE("people"),
    SCENES("scenes"),
    TECHNOLOGY("technology"),
    TRANSPORT("transport");
}